package co.com.pragma.ejercicio;

import co.com.pragma.domain.Persona;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@Slf4j
public class ControladorInicio {

    @GetMapping("/")
    public String inicio(Model modelo) {
        log.info("Ejecutando decide un controlador MVC");
        var mensaje = "Hola mundo desde thymeleaf!";
        modelo.addAttribute("mensaje",mensaje);

        // Creamos una persona
        Persona persona = new Persona();
        // Agregamos el nombre
        persona.setNombre("Alexander");
        // agregamos el apellido
        persona.setApellido("Fonseca");
        // Agregamos el telefono
        persona.setTelefono("3123036763");
        // agregamos el correo
        persona.setEmail("alexader.fonseca@pragma.com.co");

        // Creamos una persona 2
        Persona persona2 = new Persona();
        // Agregamos el nombre
        persona2.setNombre("Yaritza");
        // agregamos el apellido
        persona2.setApellido("Molina");
        // Agregamos el telefono
        persona2.setTelefono("3123036587");
        // agregamos el correo
        persona2.setEmail("yaritzamolina@gmail.com");

        /*
        var personas = new ArrayList<>();
        personas.add(persona);
        personas.add(persona2);
        */
        // enviamos una lista vacia para que se cumpla la validacion
        //var personas = Arrays.asList();
        // enviamos una lista con objetos que se enviaran a la vista
        var personas = Arrays.asList(persona,persona2);

        modelo.addAttribute("personas",personas);
        return  "index";
    }

}
